//
//  ModelControllerTests.swift
//  PruebaTests
//
//  Created by Leandro Pardo on 11/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import XCTest
@testable import Prueba

class ModelControllerTests: XCTestCase {
    
    var modelController: ModelController?
    
    override func setUp() {
        super.setUp()
        modelController = ModelController(modelName: "Prueba")
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    override class func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testAddUser() {
        let username = "usuario"
        
        let newUser = modelController!.add(User.self)
        XCTAssertNotNil(newUser)
        
        newUser?.username = username
        XCTAssertEqual(username, newUser?.username, "Username is not set")
    }
    
    func testTotalUsers() {
        
        let newUser = modelController!.add(User.self)
        newUser?.username = "usuario"
        
        let total = modelController!.total(User.self)
        XCTAssertGreaterThan(total, 0)
    }
    
    func testFetchUsers() {
        let username = "usuario"
        let newUser = modelController!.add(User.self)
        newUser?.username = username
        
        let predicate = NSPredicate(format: "username LIKE %@", username)
        let fetched = modelController!.fetch(User.self, predicate: predicate)
        
        guard let fetchedUser = fetched?.last else {
            XCTAssertNotNil(fetched)
            return
        }
        XCTAssertEqual(fetchedUser, newUser, "The fetched User does not equal the created User!")
    }
    
}
