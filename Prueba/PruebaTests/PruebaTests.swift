//
//  PruebaTests.swift
//  PruebaTests
//
//  Created by Leandro Pardo on 10/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import XCTest
@testable import Prueba

class PruebaTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: - Tests

    func testUserDao() {
        let dao = UserDAO()
        let fetched = dao.findByUsername(username: "user1")
        XCTAssertNotNil(fetched, "The preloaded user was not found")
    }
    
    func testUserDaoNotFound() {
        let dao = UserDAO()
        let fetched = dao.findByUsername(username: "usuario")
        XCTAssertNil(fetched, "An invalid user was found")
    }
    
    func testPerformanceFetch() {
        self.measure {
            let dao = UserDAO()
            _ = dao.findByUsername(username: "user1")
        }
    }
    
    func testColourController() {
        let sut: ColourViewController? = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "ColourViewController") as? ColourViewController
        let label = UILabel()
        sut?.updateLabel(label: label, textKey: "blue", color: UIColor.blue)
        XCTAssertEqual(label.textColor, UIColor.blue)
        XCTAssertEqual(label.text, "Azul")
    }

    func testCrypto() {
        let password = "a12345"
        let pass = password.hashed(.sha256)
        XCTAssertEqual(pass, "62080f96a2bfc48794326c5b9750942d15886e6a9746fc215cd0d04127196db2", "The hashed result does not match the expected value")
    }
    
    func testPerformanceCrypto() {
        self.measure {
            let password = "a12345"
            _ = password.hashed(.sha256)
        }
    }

}
