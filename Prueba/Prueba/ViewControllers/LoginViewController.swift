//
//  ViewController.swift
//  Prueba
//
//  Created by Leandro Pardo on 10/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import UIKit
import CommonCrypto

class LoginViewController: UIViewController {

    @IBOutlet weak var userTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set gesture recognizer for keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.screenTapped))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    private func showError(titleKey: String, messageKey: String) {
        let validationAlert = UIAlertController(title: NSLocalizedString(titleKey, comment: ""), message: NSLocalizedString(messageKey, comment: ""), preferredStyle: .alert)
        validationAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(validationAlert, animated: true, completion: nil)
    }
    
    private func doLogin(username: String, password: String) {
        let dao = UserDAO()
        let fetched = dao.findByUsername(username: username)
        if (fetched != nil) {
            let pass = password.hashed(.sha256)
            if (fetched?.password == pass) {
                
                performSegue(withIdentifier: "showHomeViewController", sender: self)
            } else {
                showError(titleKey: "error", messageKey: "password_error")
            }
        } else {
            showError(titleKey: "error", messageKey: "username_error")
        }

        
    }
    
    // MARK: - TapActions
    
    @objc func screenTapped() {
        self.view.endEditing(true)
    }

    @IBAction func loginTapped(_ sender: Any?) {
        guard let username = self.userTf.text else {
            showError(titleKey: "required_field", messageKey: "empty_username")
            return
        }
        if (username.isEmpty) {
            showError(titleKey: "required_field", messageKey: "empty_username")
            return
        }
        guard let password = self.passwordTf.text else {
            showError(titleKey: "required_field", messageKey: "empty_password")
            return
        }
        if (password.isEmpty) {
            showError(titleKey: "required_field", messageKey: "empty_password")
            return
        }
        self.doLogin(username: username, password: password)
    }
    
}

// MARK: - UITextFieldDelegate

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.userTf {
            self.passwordTf.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.loginTapped(nil)
        }
        
        return true
    }
}
