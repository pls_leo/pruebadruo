//
//  AnimationViewController.swift
//  Prueba
//
//  Created by Leandro Pardo on 10/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {

    @IBOutlet weak var topCtrt: NSLayoutConstraint!
    @IBOutlet weak var bottomCtrt: NSLayoutConstraint!
    @IBOutlet weak var logoIv: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Accesibility label for testing access porpouses
        self.logoIv.accessibilityLabel = "logo"
    }
    
    // MARK: - TapActions
    
    @IBAction func animTapped(_ sender: Any) {
        UIView.animate(withDuration: 2, animations: {
            self.topCtrt.isActive = !self.topCtrt.isActive
            self.bottomCtrt.isActive = !self.bottomCtrt.isActive
    
            self.view.layoutIfNeeded()
        })
    }
}
