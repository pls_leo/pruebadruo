//
//  SumViewController.swift
//  Prueba
//
//  Created by Leandro Pardo on 10/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import UIKit

class SumViewController: UIViewController {

    @IBOutlet weak var sumLbl: UILabel!
    
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    private func increaseCounter() {
        counter += 1
    }
    
    // MARK: - TapActions

    @IBAction func sumTapped(_ sender: Any) {
        self.increaseCounter()
        sumLbl.text = "\(counter)"
    }
}
