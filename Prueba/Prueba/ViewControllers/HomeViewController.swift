//
//  HomeViewController.swift
//  Prueba
//
//  Created by Leandro Pardo on 10/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ unwindSegue: UIStoryboardSegue) {}

}

// MARK: - UITableViewDataSource

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        if (indexPath.row == 0) {
            cell.titleLbl.text = NSLocalizedString("first_screen", comment: "")
            cell.descriptionLbl.isHidden = true
            cell.chevronIv.isHidden = true
            cell.iconIv.image = UIImage(named: "creditcard")
        } else if (indexPath.row == 1) {
            cell.titleLbl.text = NSLocalizedString("second_screen", comment: "")
            cell.descriptionLbl.isHidden = true
            cell.chevronIv.isHidden = false
            cell.iconIv.image = UIImage(named: "giftcard")
        } else {
            cell.titleLbl.text = NSLocalizedString("third_screen", comment: "")
            cell.descriptionLbl.text = NSLocalizedString("description", comment: "")
            cell.descriptionLbl.isHidden = false
            cell.chevronIv.isHidden = false
            cell.iconIv.image = UIImage(named: "membershipcard")
        }
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0) {
            performSegue(withIdentifier: "showSumViewController", sender: self)
        } else if (indexPath.row == 1) {
            performSegue(withIdentifier: "showColourViewController", sender: self)
        } else {
            performSegue(withIdentifier: "showAnimationViewController", sender: self)
        }
    }
}
