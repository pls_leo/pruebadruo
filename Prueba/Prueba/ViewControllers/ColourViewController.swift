//
//  ColourViewController.swift
//  Prueba
//
//  Created by Leandro Pardo on 10/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import UIKit

class ColourViewController: UIViewController {

    @IBOutlet weak var colourLbl: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var labelCenterXCtrt: NSLayoutConstraint!
    @IBOutlet weak var labelCentrYCtrt: NSLayoutConstraint!
    @IBOutlet weak var labelLeadingCtrt: NSLayoutConstraint!
    @IBOutlet weak var labelTopCtrt: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func moveLabel() {
        self.labelCenterXCtrt.isActive = false
        self.labelCentrYCtrt.isActive = false
        self.labelLeadingCtrt.isActive = true
        self.labelTopCtrt.isActive = true
        
        let labelFrame = self.colourLbl.frame
        let navigationBarFrame = self.navigationBar.frame
        let guide = view.safeAreaLayoutGuide
        let maxX = guide.layoutFrame.size.width - labelFrame.width
        let maxY = guide.layoutFrame.size.height - labelFrame.height - navigationBarFrame.height - 40
        
        UIView.animate(withDuration: 0.25, animations: {
            self.labelLeadingCtrt.constant  = CGFloat(arc4random_uniform(UInt32(maxX)))
            self.labelTopCtrt.constant  = CGFloat(arc4random_uniform(UInt32(maxY)))
            self.view.layoutIfNeeded()
        })

    }
    
    func updateLabel(label: UILabel, textKey: String, color: UIColor) {
        label.text = NSLocalizedString(textKey, comment: "")
        label.textColor = color
    }
    
    // MARK: - TapActions
    
    @IBAction func blueTapped(_ sender: Any?) {
        self.updateLabel(label: self.colourLbl, textKey: "blue", color: UIColor.blue)
        self.moveLabel()
    }
    
    @IBAction func greenTapped(_ sender: Any?) {
        self.updateLabel(label: self.colourLbl, textKey: "green", color: UIColor.green)
        self.moveLabel()
    }

}
