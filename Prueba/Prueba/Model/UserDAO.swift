//
//  UserDAO.swift
//  Prueba
//
//  Created by Leandro Pardo on 11/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import UIKit

class UserDAO: NSObject {
    
    let modelController: ModelController?
    
    override init() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        modelController = appDelegate?.modelController
        super.init()
    }
    
    func findByUsername (username: String) -> User? {
        guard let modelController = modelController else { return nil }
        let predicate = NSPredicate(format: "username LIKE %@", username)
        let fetched = modelController.fetch(User.self, predicate: predicate)
        if (fetched != nil && fetched!.count > 0) {
            return fetched![0]
        }
        return nil
    }

}
