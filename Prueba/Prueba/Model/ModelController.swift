//
//  ModelController.swift
//  Prueba
//
//  Created by Leandro Pardo on 11/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import UIKit
import CoreData

public class ModelController {
    
    public typealias VoidCompletion = () -> ()
    
    private var modelName: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: modelName)
        
        var persistentStoreDescriptions: NSPersistentStoreDescription
        
        let storeUrl = self.getDocumentsDirectory().appendingPathComponent("Prueba.sqlite")
        
        if self.isFirstLaunch() {
            let seededDataUrl = Bundle.main.url(forResource: "Prueba", withExtension: "sqlite")
            try! FileManager.default.copyItem(at: seededDataUrl!, to: storeUrl)
            let storeUrl2 = self.getDocumentsDirectory().appendingPathComponent("Prueba.sqlite-shm")
            let seededDataUrl2 = Bundle.main.url(forResource: "Prueba", withExtension: "sqlite-shm")
            try! FileManager.default.copyItem(at: seededDataUrl2!, to: storeUrl2)
            let storeUrl3 = self.getDocumentsDirectory().appendingPathComponent("Prueba.sqlite-wal")
            let seededDataUrl3 = Bundle.main.url(forResource: "Prueba", withExtension: "sqlite-wal")
            try! FileManager.default.copyItem(at: seededDataUrl3!, to: storeUrl3)
        }
        
        let description = NSPersistentStoreDescription()
        description.shouldInferMappingModelAutomatically = true
        description.shouldMigrateStoreAutomatically = true
        description.url = storeUrl
        
        container.persistentStoreDescriptions = [description]
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var context: NSManagedObjectContext = {
        return persistentContainer.viewContext
    }()
    
    // MARK: - Convenience Init
    
    public convenience init(modelName model: String) {
        self.init()
        modelName = model
    }
    
    func isFirstLaunch() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        if (isFirstLaunch) {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.synchronize()
        }
        return isFirstLaunch
    }
    
    func getDocumentsDirectory()-> URL {
        let paths = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}

// MARK: - Add
extension ModelController {
    
    public func add<M: NSManagedObject>(_ type: M.Type) -> M? {
        
        var modelObject: M?
        
        if let entity = NSEntityDescription.entity(forEntityName: M.description(), in: context) {
            modelObject = M(entity: entity, insertInto: context)
        }
        return modelObject
    }
}


// MARK: - Fetch
extension ModelController {
    
    public typealias TotalCompletion = ((_ count: Int) -> ())
    public typealias FetchCompletion<M> = ((_ fetched: [M]) -> ())
    
    public func total<M: NSManagedObject>(_ type: M.Type, _ completion: TotalCompletion?=nil) -> Int {
        
        var count = 0
        let entityName = String(describing: type)
        let request = NSFetchRequest<M>(entityName: entityName)
        
        if let completion = completion {
            self.context.perform {
                let _ = self.fetchTotal(request, completion)
            }
        }
        else {
            count = fetchTotal(request)
        }
        return count
    }
    
    private func fetchTotal<M: NSManagedObject>(_ request: NSFetchRequest<M>,
                                                _ completion: TotalCompletion?=nil) -> Int {
        var count = 0
        do {
            count = try context.count(for: request)
            completion?(count)
        }
        catch {
            print("Error executing total: \(error)")
            completion?(0)
        }
        return count
    }
    
    public func fetch<M: NSManagedObject>(_ type: M.Type,
                                          predicate: NSPredicate?=nil,
                                          sort: NSSortDescriptor?=nil,
                                          _ completion: FetchCompletion<M>?=nil) -> [M]? {
        var fetched: [M]?
        let entityName = String(describing: type)
        let request = NSFetchRequest<M>(entityName: entityName)
        
        request.predicate = predicate
        request.sortDescriptors = [sort] as? [NSSortDescriptor]
        
        if let completion = completion {
            self.context.perform {
                let _ = self.fetchObjects(request, completion)
            }
        }
        else {
            fetched = fetchObjects(request)
        }
        return fetched
    }
    
    private func fetchObjects<M: NSManagedObject>(_ request: NSFetchRequest<M>,
                                                  _ completion: FetchCompletion<M>?=nil) -> [M] {
        var fetched: [M] = [M]()
        
        do {
            fetched = try context.fetch(request)
            completion?(fetched)
        }
        catch {
            print("Error executing fetch: \(error)")
            completion?(fetched)
        }
        return fetched
    }
}

// MARK: - Save
extension ModelController {
    
    public func save(_ completion: VoidCompletion?=nil) {
        
        context.perform {
            if self.context.hasChanges {
                do {
                    try self.context.save()
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
            completion?()
        }
    }
}
