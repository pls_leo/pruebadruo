//
//  FirstScreenUITests.swift
//  PruebaUITests
//
//  Created by Leandro Pardo on 11/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import XCTest

class SumScreenUITests: XCTestCase {
    
    override func setUp() {
        
        continueAfterFailure = false
        
        let app = XCUIApplication()
        app.launch()
        
        let userTf = app.textFields["Usuario"]
        userTf.tap()
        userTf.typeText("user1")
        
        let passTf = app.secureTextFields["Contraseña"]
        passTf.tap()
        passTf.typeText("a12345")
        
        let loginBttn = app.buttons["Ingresar"]
        loginBttn.tap()
        
        let firstScreenCell = app.tables/*@START_MENU_TOKEN@*/.staticTexts["Primera pantalla"]/*[[".cells.staticTexts[\"Primera pantalla\"]",".staticTexts[\"Primera pantalla\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        firstScreenCell.tap()
        
    }
    
    override func tearDown() {}
    
    // MARK: - Tests
    
    func testSum() {
        
        let app = XCUIApplication()
        
        XCTAssertTrue(app.staticTexts["0"].exists, "Label text was not found")
        
        let sumBttn = app.buttons["Sumar"]
        XCTAssertTrue(sumBttn.exists, "The sum button was not found")
        sumBttn.tap()
        
        XCTAssertTrue(app.staticTexts["1"].exists, "Label text was not updated")
        
    }

}
