//
//  ColourScreenUITests.swift
//  PruebaUITests
//
//  Created by Leandro Pardo on 11/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import XCTest

class ColourScreenUITests: XCTestCase {
    
    override func setUp() {
        
        continueAfterFailure = false
        
        let app = XCUIApplication()
        app.launch()
        
        let userTf = app.textFields["Usuario"]
        userTf.tap()
        userTf.typeText("user1")
        
        let passTf = app.secureTextFields["Contraseña"]
        passTf.tap()
        passTf.typeText("a12345")
        
        let loginBttn = app.buttons["Ingresar"]
        loginBttn.tap()
        
        let firstScreenCell = app.tables.staticTexts["Segunda pantalla"]
        firstScreenCell.tap()
        
    }
    
    override func tearDown() {}
    
    // MARK: - Tests

    func testBlue() {
        
        let app = XCUIApplication()
        let label = app.staticTexts["0"]
        XCTAssertTrue(label.exists, "Label text was not found")
        let originalFrame = label.firstMatch.frame
        
        let sumBttn = app.buttons["Azul"]
        XCTAssertTrue(sumBttn.exists, "The blue button was not found")
        sumBttn.tap()
        
        let blueLbl = app.staticTexts["Azul"]
        XCTAssertTrue(blueLbl.exists, "Label text was not updated")
        
        let updatedFrame = blueLbl.firstMatch.frame
        
        XCTAssertNotEqual(originalFrame.origin.x, updatedFrame.origin.x, "Label was not moved")
        XCTAssertNotEqual(originalFrame.origin.y, updatedFrame.origin.y, "Label was not moved")
    }
    
    
    func testGreen() {
        
        let app = XCUIApplication()
        let label = app.staticTexts["0"]
        XCTAssertTrue(label.exists, "Label text was not found")
        let originalFrame = label.firstMatch.frame
        
        let sumBttn = app.buttons["Verde"]
        XCTAssertTrue(sumBttn.exists, "The green button was not found")
        sumBttn.tap()
        
        let blueLbl = app.staticTexts["Verde"]
        XCTAssertTrue(blueLbl.exists, "Label text was not updated")
        
        let updatedFrame = blueLbl.firstMatch.frame
        
        XCTAssertNotEqual(originalFrame.origin.x, updatedFrame.origin.x, "Label was not moved")
        XCTAssertNotEqual(originalFrame.origin.y, updatedFrame.origin.y, "Label was not moved")
    }
}
