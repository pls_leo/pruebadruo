//
//  HomeUITests.swift
//  PruebaUITests
//
//  Created by Leandro Pardo on 11/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import XCTest

class HomeUITests: XCTestCase {
    
    override func setUp() {
        
        continueAfterFailure = false
        
        let app = XCUIApplication()
        app.launch()
        
        let userTf = app.textFields["Usuario"]
        userTf.tap()
        userTf.typeText("user1")
        
        let passTf = app.secureTextFields["Contraseña"]
        passTf.tap()
        passTf.typeText("a12345")
        
        let loginBttn = app.buttons["Ingresar"]
        loginBttn.tap()
    }
    
    override func tearDown() {}
    
    // MARK: - Tests
    
    func testFirstScreen() {
        
        let app = XCUIApplication()
        
        let firstScreenCell = app.tables/*@START_MENU_TOKEN@*/.staticTexts["Primera pantalla"]/*[[".cells.staticTexts[\"Primera pantalla\"]",".staticTexts[\"Primera pantalla\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        XCTAssertTrue(firstScreenCell.exists, "First screen cell not found")
        firstScreenCell.tap()
        
        XCTAssertTrue(app.navigationBars["Primera pantalla"].exists, "First screent not showed")
       
    }
    
    func testSecondScreen() {
        
        let app = XCUIApplication()
        
        let firstScreenCell = app.tables.staticTexts["Segunda pantalla"]
        XCTAssertTrue(firstScreenCell.exists, "Second screen cell not found")
        firstScreenCell.tap()
        
        XCTAssertTrue(app.navigationBars["Segunda pantalla"].exists, "Second screent not showed")
        
    }
    
    func testThirdScreen() {
        
        let app = XCUIApplication()
        
        let firstScreenCell = app.tables.staticTexts["Tercera pantalla"]
        XCTAssertTrue(firstScreenCell.exists, "Third screen cell not found")
        firstScreenCell.tap()
        
        XCTAssertTrue(app.navigationBars["Tercera pantalla"].exists, "Third screent not showed")
        
    }
}
