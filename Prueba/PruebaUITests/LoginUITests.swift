//
//  PruebaUITests.swift
//  PruebaUITests
//
//  Created by Leandro Pardo on 10/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import XCTest

class LoginUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false

        XCUIApplication().launch()
    }

    override func tearDown() {}
    
    // MARK: - Tests

    func testEmptyUsername() {
        
        let app = XCUIApplication()
        let loginBttn = app.buttons["Ingresar"]
        XCTAssertTrue(loginBttn.exists, "The login button was not found")
        loginBttn.tap()
        
        let userErrorAlert = app.alerts["Campo requerido"]
        XCTAssertTrue(userErrorAlert.staticTexts["El campo usuario no puede estar vacío"].exists)
    }
    
    func testEmptyPassword() {
        
        let app = XCUIApplication()

        let userTf = app.textFields["Usuario"]
        XCTAssertTrue(userTf.exists, "The username textfield was not found")
        userTf.tap()
        userTf.typeText("user")
        
        let loginBttn = app.buttons["Ingresar"]
        loginBttn.tap()
        
        let passErrorAlert = app.alerts["Campo requerido"]
        XCTAssertTrue(passErrorAlert.staticTexts["El campo contraseña no puede estar vacío"].exists)

    }
    
    func testInvalidUser() {
        let app = XCUIApplication()
        
        let userTf = app.textFields["Usuario"]
        userTf.tap()
        userTf.typeText("user")
        
        let passTf = app.secureTextFields["Contraseña"]
        XCTAssertTrue(passTf.exists, "The password textfield was not found")
        passTf.tap()
        passTf.typeText("a123")
        
        let loginBttn = app.buttons["Ingresar"]
        loginBttn.tap()
        
        let passErrorAlert = app.alerts["Error"]
        XCTAssertTrue(passErrorAlert.staticTexts["El usuario es incorrecto"].exists)
        
        
    }
    
    func testInvalidPassword() {
        let app = XCUIApplication()
        
        let userTf = app.textFields["Usuario"]
        userTf.tap()
        userTf.typeText("user1")
        
        let passTf = app.secureTextFields["Contraseña"]
        passTf.tap()
        passTf.typeText("a123")
        
        let loginBttn = app.buttons["Ingresar"]
        loginBttn.tap()
        
        let passErrorAlert = app.alerts["Error"]
        XCTAssertTrue(passErrorAlert.staticTexts["La contraseña es incorrecta"].exists)
        
        
    }

    func testValidLogin() {
        let app = XCUIApplication()
        
        let userTf = app.textFields["Usuario"]
        userTf.tap()
        userTf.typeText("user1")
        
        let passTf = app.secureTextFields["Contraseña"]
        passTf.tap()
        passTf.typeText("a12345")
        
        let loginBttn = app.buttons["Ingresar"]
        loginBttn.tap()
    
        XCTAssertTrue(app.tables/*@START_MENU_TOKEN@*/.staticTexts["Primera pantalla"]/*[[".cells.staticTexts[\"Primera pantalla\"]",".staticTexts[\"Primera pantalla\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.exists, "Login not pass")
    }
}
