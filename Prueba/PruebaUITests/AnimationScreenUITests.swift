//
//  AnimationScreenUITests.swift
//  PruebaUITests
//
//  Created by Leandro Pardo on 11/07/2019.
//  Copyright © 2019 Druo. All rights reserved.
//

import XCTest

class AnimationScreenUITests: XCTestCase {
    
    override func setUp() {
        
        continueAfterFailure = false
        
        let app = XCUIApplication()
        app.launch()
        
        let userTf = app.textFields["Usuario"]
        userTf.tap()
        userTf.typeText("user1")
        
        let passTf = app.secureTextFields["Contraseña"]
        passTf.tap()
        passTf.typeText("a12345")
        
        let loginBttn = app.buttons["Ingresar"]
        loginBttn.tap()
        
        let firstScreenCell = app.tables.staticTexts["Tercera pantalla"]
        firstScreenCell.tap()
        
    }
    
    override func tearDown() {}
    
    // MARK: - Tests

    func testAnimation() {
        
        let app = XCUIApplication()
        
        let button = app.navigationBars["Tercera pantalla"].children(matching: .button).element(boundBy: 1)
        XCTAssertTrue(button.exists, "Animation button not found")
        
        let image = app.images["logo"]
        XCTAssertTrue(image.exists, "Image not found")
        
        let originalY = image.frame.origin.y
        
        button.tap()
        
        var updatedY = image.frame.origin.y
        
        XCTAssertNotEqual(originalY, updatedY, "Image was not moved")
        
        button.tap()
        
        updatedY = image.frame.origin.y
        
        XCTAssertEqual(originalY, updatedY, "Image was not moved back to original position")
        
    }

}
